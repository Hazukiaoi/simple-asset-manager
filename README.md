# Simple Asset Manager

Simple Asset Manager for Blender 2.8. Works with **objects**, **materials**, **hdr** and **particle systems**.
Did this one due to dealys for internal asset manager for Blender.

**Made for simplicity of usage.**

- Opens `blend`, `obj`, `fbx`, `hdr`, `exr` from the same place;
- Dynamic, and custom categories: Except of `material`, and `particle` this addon will follow your library layout, folders and subfolders;
- absolute or relative libraries folder;
- creates structured collections: `Assets`, `Assets/Particles`;
- With `Ctrl+scroll` over the categories it is even faster;
- Small and simple UI. Simple, and small to modify code as you like;
- option to batch render all previews;
- works as operator too (One can search `Simple Asset Manager` in command search, and assign a SHORTCUT!! :) But there is no default shortcut assigned for it);
- search bar.

None of the mangers I've tried was enough for me. Asset flinger was nice, but the idea with previews was much better. Chocofur asset manager is almost perfect, but still, it had its cons: separate spaces for categories, and (i only assume) fixed categories.

Focused on simplicity. Less buttons more functionality.

## Install

[Download this link SimpleAssetManager.zip file](https://gitlab.com/tibicen/simple-asset-manager/raw/master/SimpleAssetManager.zip)

Install .zip file without unpacking. Choose Library folder with your blends.

## Usage

After install you can also mark to render missing previews. It will render material view with eevee of your models, materials, and particle systems in one specific manner. Of course you can always render your own previews as 200x200 .png with the same name as blend file. But remember it can take awhile.

Manager tab is located in `Viewport tool panel` (shortcut key: `N`).
You will see all your folders as categories, and if any folder has additional folders you will see them as subcategory. For simplicity i didn't add any additinal ones.

**For simple usage i made some hacks.** So in folder named `materials` the addon will only import materials, and if the folder is called `particles`, then it will import all the particle systems. All the rest of the folders will import you blend, obj, fbx models, and hdr images.


![Small and handy.](preview.gif "Small and handy")

## Changelog
- 0.9.5 - search bar, relative library path, fix non-english characters, fix at origin import, added rotation, API updates.
- 0.9.4 - fixed append at origin issue, particles when imported again use previous import group.
- 0.9.3 - UI popup operator (shortcut)
- 0.9.0 - fixed silent enum error, improts cllections from blend files, imports particles as groups, can choose exr for render prev, fix render prevs.
- 0.8.0 - materials can be `append`, `added` and `replaced`.
- 0.7.0 - `open` opens file in separate instance, as well as `render previews`.
- 0.6.0 - added exr format for env images, asset collection!
- 0.5.0 - fix material shpere UV.
- 0.4.0 - fix exr, fir particle render prev, updated api, fix hdr duplicate import.
- 0.3.0 - adds obj, fbx, hdr handling, hides unnecesarry buttons.
- 0.2.0 - adds at cursor placement, minor typos, fixes importing objects with materials.


## TODO:
- most important: 0.9.6 will import obj into Assets collection, and create instance of it.
- create master library, and per project library override.
- (!) search option!;
- (?) add licence info (would be nice to add separate licence parameter for each object in file, then with one click can generate credits for everyone).
